CREATE TABLE Heart (
	id 				INTEGER PRIMARY KEY AUTOINCREMENT, 
	device_id		TEXT, 
	heart_rate		REAL, 
	accuracy		INTEGER,
	steps 			INTEGER, 
	timestamp		INTEGER
);