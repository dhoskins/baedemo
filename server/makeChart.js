(function() {
	var moment = require('moment');
	var toTwo = function(i) {
		if (i<10) { return '0' + i; }
		else { return i; }
	}

	exports.getStartTime = function(now, readingsPerMin) {
		now.milliseconds(0);
		var secs = now.seconds();
		var readingsEachSec = 60 / readingsPerMin;
		secs -= (secs % readingsEachSec);
		now.seconds(secs);
		return now;
	};

	exports.makeTrendPoints = function(mins, startTime, readingsPerMin) {
		var arr = [];
		var readingsEachSec = 60 / readingsPerMin;

		var curTime = moment(startTime).subtract(mins, 'minutes');

		for (var i=0, len=(mins * readingsPerMin); i<len; i++) {
			//var time = moment(curTime).subtract((mins - i), 'minutes');
			var time = moment(curTime);
			if ( time.seconds() === 0 ) {
				arr.push(toTwo(time.hours()) + ':' + toTwo(time.minutes()));				
			} else {
				arr.push('');
			}
			curTime.add(readingsEachSec, 'seconds');
		}
		return arr;
	};

	function add(a, b) {
	    return a + b;
	};

	exports.makeTrendFromDb = function(obj, totalMinutes) {
		var res = {};
		for (var id in obj) {
			if (obj.hasOwnProperty(id)) {
				var avg = {};
				var values = obj[id];
				values.forEach(function(v) {
					if (!avg[v.blocks]) {
						avg[v.blocks] = [];
					}
					avg[v.blocks].push(v.heart_rate);
				});

				res[id] = [];
				var last = -1;
				var first = -1;
				for (var i=0; i<totalMinutes; i++) {
					var reverse = totalMinutes - i - 1
					if (avg[reverse]) {
						var average = Math.round(avg[reverse].reduce(add, 0) / avg[reverse].length);
						res[id].push(average);
						last = average;
						if (first === -1) {
							first = average;
						}
					} else {
						res[id].push(last);
					}
				}

				for (var i=0; i<res[id].length; i++) {
					var val = res[id][i];
					if (val === -1) {
						res[id][i] = first;
					} else {
						break;
					}
				}
			}
		}

		return res;
	};

	var startTime = moment('2013-02-08 09:30:26');

	//console.log(exports.getStartTime(startTime, 4));
	//console.log(exports.makeTrendPoints(12, startTime, 4));
	var r = exports.makeTrendFromDb({
		1: [
			{
				heart_rate:70,
				blocks:4
			},
			{
				heart_rate:80,
				blocks:4
			},
			{
				heart_rate:90,
				blocks:10,
			}
		],
		2: [
			{
				heart_rate:100,
				blocks:0
			},
			{
				heart_rate:200,
				blocks:0
			}
		]
	}, 12);
	console.log(r);
})();

