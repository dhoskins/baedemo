(function() {
	var retrieve = function(callback) {
		window.setTimeout(function() { retrieve(callback); }, 10000);

		var xhr = new XMLHttpRequest();
		var reqListener = function() {
			var resp = JSON.parse(xhr.response);
			callback(resp);
		};
		xhr.addEventListener('load', reqListener);
		xhr.open('GET', '/clientData');
		xhr.send();
	};

	exports.initialize = function(callback) {
		retrieve(callback);
	};
})();