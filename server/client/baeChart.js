(function() {
	var canvas = document.getElementById('mainChart');
	var context = canvas.getContext('2d');
	var userColors = require('./userColors.js')
	var Chart, chart;
	var datasets;

	var initDataset = function(storeIndex, name) {
		var lg = context.createLinearGradient(0, 0, 0, 400);
		var color = userColors.colors()[storeIndex];
		var rgba = 'rgba(' + color[0] + ',' + color[1] + ',' + color[2];
		lg.addColorStop(0, rgba + ',1)');
		lg.addColorStop(1, rgba + ',0)');

		var dataset = {};
		dataset.label = name;
		dataset.data = [];
		dataset.borderColor = rgba + ',1)';
		dataset.backgroundColor = lg;
		//dataset.tension = 0;
		return dataset;
	};

	var initChart = function(data) {
		Chart = require('chart.js');
		Chart.defaults.global.defaultFontFamily = 'Lato';
		Chart.defaults.global.defaultFontColor = '#fff';
		var labels = data.trendPoints.slice();

		var userData = data.userData;
		datasets = [ ];

		for (var userId in userData) {
			if (userData[userId].trend) {
				if (userData.hasOwnProperty(userId)) {
					var user = userData[userId];
					var storeIndex = userColors.getLocalUserId(user.snapshot);
					datasets.push(initDataset(storeIndex, user.snapshot.name));
				}
			}
		}

		chart = new Chart(context, {
			type: 'line',
			data: {
				datasets: datasets,
				labels: []
			},
			options: {
				title: {
					display: true,
					text: 'HEART RATE BPM',
					fontSize: 18
				},
				legend: {
					display: false
				},
				scales: {
					xAxes: [ {
						gridLines: {
							display: false
						}
					} ],
					yAxes: [ {
						position: 'right',
						gridLines: {
							display: true,
							color: '#545454'
						},
						ticks: {
							min: 40,
							max: 200
						},
						fontSize: 8
					} ]
				}
			}
		});

		updateChart(data);
	};

	
	var updateChart = function(data) {
		chart.data.labels = data.trendPoints.slice();
		var userData = data.userData;
		for (var userIndex in userData) {
			if (userData.hasOwnProperty(userIndex)) {
				if (userData[userIndex].trend) {
					var user = userData[userIndex];
					var storeIndex = userColors.getLocalUserId(user.snapshot);
					if (!datasets[storeIndex]) {
						datasets[storeIndex] = initDataset(storeIndex, user.snapshot.name);
					}
					datasets[storeIndex].data = user.trend.slice();
				}
			}
		}
		chart.data.datasets = datasets;
		chart.update();
	};

	exports.setData = function(data) {
		console.log(data);

		if (!Chart) {
			initChart(data);
		} else {
			updateChart(data);
		}
	};
})();