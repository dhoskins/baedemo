(function() {
	require('./time.js').initialize(function(date, day, time) {
		document.getElementById('date').innerHTML = date;
		document.getElementById('day').innerHTML = day;
		document.getElementById('time').innerHTML = time;
	});

	require('./retrieve.js').initialize(function(val) {
		require('./statusBoxes.js').setUsers(val);
		require('./baeChart.js').setData(val);
	});
})();