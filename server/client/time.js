(function() {
	var moment = require('moment');
	var update = function(callback) {
		window.setTimeout(function() { update(callback); }, 1000);
		callback(moment().format('Do MMMM YYYY'), moment().format('dddd').toUpperCase(), moment().format('LT'));
	};
	exports.initialize = function(callback) {
		update(callback);
	};
})();