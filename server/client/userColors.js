(function() {
	exports.colors = function() {
					/* blue */       /* green */        /* purple */     /* orange */
		return [ [ 40, 205, 224 ], [ 51, 188, 113 ], [ 155, 67, 189 ], [ 247, 146, 49 ] ];
	};

	var localUserStore = [];
	exports.getLocalUserId = function(user) {
		var userId = user.id;
		var storeIndex = localUserStore.indexOf(userId);
		if (storeIndex === -1) {
			localUserStore.push(userId);
		}
		return localUserStore.indexOf(userId);
	};
})();