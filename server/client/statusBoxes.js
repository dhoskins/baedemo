(function() {
	var boxes = {};
	var userColors = require('./userColors.js');

	var div = function(cssClass, parent, innerHTML) {
		var div = document.createElement('div');
		div.setAttribute('class', cssClass);
		if (parent) { parent.appendChild(div); }
		if (innerHTML) { div.innerHTML = innerHTML; }
		return div;
	}

	var setupNewBox = function(parent) {
		var newBox = div('status_box', parent);
		var status_top = div('status_top', newBox);
		var status_pic = div('status_pic', status_top);
		var status_name_age = div('status_name_age', status_top);
		var status_name = div('status_name', status_name_age);
		var status_age = div('status_age', status_name_age);
		var status_bottom = div('status_bottom', newBox);
		var status_heart = div('status_stat status_heart', status_bottom);
		var status_heart_val_container = div('status_val_container', status_heart);
		div('status_val_logo', status_heart_val_container, '♥');
		var status_heart_val = div('status_val', status_heart_val_container);
		div('status_caption', status_heart, 'BPM');
		var status_hrmax = div('status_stat status_hrmax', status_bottom);
		var status_hrmax_val_container = div('status_val_container', status_hrmax);
		var status_hrmax_val_logo = div('status_val_logo', status_hrmax_val_container);
		var status_hrmax_val = div('status_val', status_hrmax_val_container);
		div('status_caption', status_hrmax, 'HRMAX');
		var status_steps = div('status_stat status_steps', status_bottom);
		var status_steps_val_container = div('status_val_container', status_steps);
		div('status_val_logo', status_steps_val_container, '<img src=\'img/Steps.png\'>');
		var status_steps_val = div('status_val', status_steps_val_container);
		div('status_caption', status_steps, 'STEPS');
		return function(user) {
			console.log('status user: ');
			console.log(user);
			status_name.innerHTML = user.name;
			status_age.innerHTML = user.age + ' YEARS OLD';
			status_heart_val.innerHTML = user.heart_rate;
			status_pic.innerHTML = '<img src=\'' + user.photo + '\'>';
			status_hrmax_val.innerHTML = user.hr_max;
			switch (user.hr_max_direction) {
				case 'up':
					status_hrmax_val_logo.innerHTML = '▲';
					break;

				case 'down':
					status_hrmax_val_logo.innerHTML = '▼';
					break;

				default:
					status_hrmax_val_logo.innerHTML = '=';
					break;
			}
			status_steps_val.innerHTML = user.steps;

			var userId = userColors.getLocalUserId(user);
			var color = userColors.colors()[userId];
			status_heart.style.color = 'rgb(' + color[0] + ',' + color[1] + ',' + color[2] + ')';
		};
	};

	exports.setUsers = function(data) {
		console.log(data);
		var userData = data.userData;
		for (var id in userData) {
			if (userData.hasOwnProperty(id)) {
				var userSnapshot = userData[id].snapshot;
				if (!boxes[id]) {
					var parent = document.getElementById('status_boxes');
					var newBox = setupNewBox(parent);
					boxes[id] = newBox;
				}
				userSnapshot.id = id;
				boxes[id](userSnapshot);
			}
		}
	};
})();
