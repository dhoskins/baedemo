(function() {
	var db_filename = './bae.db';
	var fs = require('fs');
	var existed = fs.existsSync(db_filename);

	var sqlite3 = require('sqlite3').verbose();
	var db = new sqlite3.Database(db_filename);
	var moment = require('moment');
	var makeChart = require('./makeChart.js');

	exports.initialize = function(ready) {
		if (!existed) {	
			fs.readFile('./res/create_tables.sql', 'utf8', function(err, data) {
				console.log('Initializing database:');
				if (err) { return console.log(err); }
				db.exec(data);
				ready();
			});
		} else {
			ready();
		}
	};

	exports.latestHeartRates = function(res) {
		var arr = [];
		db.each('SELECT * FROM Heart WHERE timestamp IN (SELECT max(timestamp) FROM Heart GROUP BY device_id)', function(err, row) {
			arr[row.device_id] = { heart_rate: row.heart_rate, steps: row.steps };
		}, function() {
			res(arr);
		});
	};

	exports.graphs = function(cb, minsOnGraph, readingsPerMin) {
		var startTime = makeChart.getStartTime(moment(), readingsPerMin);
		var twelveMinutes = moment(startTime).subtract(minsOnGraph, 'minutes');
		var trendPoints = makeChart.makeTrendPoints(minsOnGraph, startTime, readingsPerMin);
		var obj = {};
		db.each(
			/* 
			 * This masterpiece retrieves all heart rate readings, and puts them in to 
			 * buckets according to the minute they were received.  0 being the most recent, up to 11
			 */
			'SELECT device_id, heart_rate, ((? - timestamp) / 15000) AS block FROM Heart WHERE timestamp > ? AND timestamp < ?', 
			[ startTime.format('x'), twelveMinutes.format('x'), startTime.format('x') ], 
			function(err, row) {
				if (!obj.hasOwnProperty(row.device_id)) {
					obj[row.device_id] = [];
				}
				obj[row.device_id].push({ heart_rate: row.heart_rate, blocks: row.block });

			}, function() {
				var res = makeChart.makeTrendFromDb(obj, minsOnGraph * readingsPerMin);
				cb(res);
			});
	};

	exports.queryDb = function() {
		console.log('Heart:');
		db.each('SELECT * FROM Heart', function (err, row) {
	    	console.log(row);
		});

		console.log('Steps:');
		db.each('SELECT * FROM Steps', function (err, row) {
	    	console.log(row);
		});
	};

	exports.updateHeart = function(data) {
		console.log('updating DB');
		db.run('INSERT INTO Heart (device_id, heart_rate, accuracy, steps, timestamp) VALUES (?, ?, ?, ?, ?)',
			[ data.userId, data.heartRate, data.accuracy, data.steps, data.timestamp ]);
	};
})();
