var express = require('express');
var browserify = require('browserify-middleware');
var app = express();
var bodyParser = require('body-parser')
var baeDb = require('./baeDb.js');
var makeChart = require('./makeChart.js');
var moment = require('moment');

require('./users.js').initialize(function (users) {
    baeDb.initialize(function() {
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({
            extended: true
        }));

        app.use(express.static(__dirname + '/public'));
        app.get('/bundle.js', browserify('client/baeBrowser.js'));
        app.get('/gah', function(req, res) {
            res.send('Hello World');
            baeDb.queryDb();
        });

        app.get('/usercount', function(req, res) {
            res.send(''+users.length);
        })

        var lastHrMax = {};

        app.get('/clientData', function(req, res) {
            var minsOnGraph = 12;
            var readingsPerMin = 4;

            var calcHrMax = function(age, rate) {
                var offset = 220-age;
                return Math.round((rate / offset) * 100) + '%';
            };

            var startTime = makeChart.getStartTime(moment(), readingsPerMin);
            var obj = {
                trendPoints: makeChart.makeTrendPoints(minsOnGraph, startTime, readingsPerMin),
                userData: {}
            };            
            baeDb.latestHeartRates(function(latestRates) {
                users.forEach(function (user) {
                    if (latestRates[user.device_id]) {
                        var dob = moment(user.dob);
                        var age = moment().diff(dob, 'years');
                        
                        var user_stats = latestRates[user.device_id];
                        var hr_max = calcHrMax(age, user_stats.heart_rate);
                        var hr_max_direction;
                        var last_hr_max = lastHrMax[user.device_id];
                        if (last_hr_max) {
                            if (hr_max < last_hr_max) {
                                hr_max_direction = 'down';
                            } else if (hr_max === last_hr_max) {
                                hr_max_direction = 'same';
                            } else {
                                hr_max_direction = 'up';
                            }
                        } else {
                            hr_max_direction = 'up';
                        }
                        lastHrMax[user.device_id] = hr_max;
                        
                        var photo = user.photo;
                        // If it's not a URL then look in the 'img/' folder
                        if (user.photo.indexOf('http') !== 0) {
                            photo = 'img/' + photo;
                        }
                        obj.userData[user.device_id] = {
                            snapshot: {
                                name: user.name,
                                age: age,
                                heart_rate: user_stats.heart_rate,
                                hr_max: hr_max,
                                hr_max_direction: hr_max_direction,
                                photo: photo,
                                steps: user_stats.steps || '---'
                            }
                        }
                    }
                });


                baeDb.graphs(function(graphData) {
                    for (var id in graphData) {
                        if (graphData.hasOwnProperty(id)) {
                            var gd = graphData[id];
                            obj.userData[id].trend = gd.slice();
                        }
                    }
                    res.send(JSON.stringify(obj));
                }, minsOnGraph, readingsPerMin);

            });
            
        });

        app.post('/heart', function(req, res) {
            console.log('here\'s the heart data');
            for (var i in req.body) {
                if (req.body.hasOwnProperty(i)) {
                    console.log('req.body.' + i + ' = ' + req.body[i]);
                }
            }
            if (!req.body.userId || !req.body.heartRate || !req.body.accuracy) {
                console.log('not ok');
                res.send('Not OK, missing some elements');
                return;
            }

            var heartData = {
                userId: req.body.userId,
                heartRate: req.body.heartRate,
                accuracy: req.body.accuracy,
                steps: req.body.steps
            };
            if (heartData.accuracy < 1) {
                // Discard if accuracy is below 1
                return;
            }
            heartData.timestamp = +moment();
            baeDb.updateHeart(heartData);
            res.send('OK');
        });

        app.listen(5432);
    });
})

