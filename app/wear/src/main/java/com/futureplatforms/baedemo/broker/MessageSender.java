package com.futureplatforms.baedemo.broker;

import com.futureplatforms.baedemo.RunnableParam;

/**
 * Created by douglashoskins on 12/07/2016.
 */
public interface MessageSender {
    void sendMessage(byte[] data, final Runnable onSuccess, RunnableParam<String> onError);
}
