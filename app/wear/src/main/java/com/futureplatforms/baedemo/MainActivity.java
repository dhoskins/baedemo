package com.futureplatforms.baedemo;

import android.Manifest.permission;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaCodec.BufferInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.futureplatforms.baedemo.broker.BAEView;
import com.futureplatforms.baedemo.broker.BAEView.Status;
import com.futureplatforms.baedemo.broker.SensorNetworkBroker;

public class MainActivity extends Activity {

    private static final int PermConst = 1337;
    TextView mStatus, mHeartRate;
    private Button retryConnectionButton;

    private LinearLayout chooser;
    private Button btn1, btn2, btn3;
    private boolean showChooser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener((WatchViewStub s) -> {
            chooser = (LinearLayout) s.findViewById(R.id.id_chooser);
            OnClickListener ocl = view -> {
                String id;
                if (view == btn1) {
                    id = "1";
                } else if (view == btn2) {
                    id = "2";
                } else {
                    id = "3";
                }

                chooser.setVisibility(View.GONE);

                Intent i = new Intent(this, BAEService.class);
                i.putExtra("id", id);
                startService(i);
            };

            btn1 = (Button) s.findViewById(R.id.btn1);
            btn1.setOnClickListener(ocl);
            btn2 = (Button) s.findViewById(R.id.btn2);
            btn2.setOnClickListener(ocl);
            btn3 = (Button) s.findViewById(R.id.btn3);
            btn3.setOnClickListener(ocl);


            mStatus = (TextView) s.findViewById(R.id.status);
            mHeartRate = (TextView) s.findViewById(R.id.heartRate);
            retryConnectionButton = (Button) findViewById(R.id.retry_connect);
            retryConnectionButton.setOnClickListener((View v) -> {
                proceed();
                retryConnectionButton.setVisibility(View.GONE);
            });
            if (showChooser) {
                chooser.setVisibility(View.VISIBLE);
            }
        });
    }

    private BroadcastReceiver sensor = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("Broadcast received");
            float hr = intent.getFloatExtra("HeartRate", 0.0f);
            int acc = intent.getIntExtra("Accuracy", 0);
            int steps = intent.getIntExtra("Steps", 0);

            MainActivity.this.heartSampleTaken(hr, acc, steps);
        }
    };

    private BroadcastReceiver status = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Status status = Status.values()[intent.getIntExtra("Status", Status.Error.ordinal())];
            String str = intent.getStringExtra("Text");

            setStatus(status, str);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        checkPermissions();
        System.out.println("MainActivity.onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(sensor, new IntentFilter("BAESensorData"));
        LocalBroadcastManager.getInstance(this).registerReceiver(status, new IntentFilter("BAEStatus"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("MainActivity.onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(sensor);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(status);
    }

    private void checkPermissions() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, permission.BODY_SENSORS);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{ permission.BODY_SENSORS },
                    PermConst);
        } else {
            proceed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == PermConst) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                proceed();
                return;
            }
        }
        Toast.makeText(this, "Please accept permissions!!", Toast.LENGTH_LONG);
        checkPermissions();
    }


    private void proceed() {
        if (!BAEService.IS_RUNNING) {
            if (chooser != null) {
                chooser.setVisibility(View.VISIBLE);
            } else {
                showChooser = true;
            }
        }
    }

    private void setStatus(Status s, String text) {
        int color;
        String str;
        final String finalStr;
        switch (s) {
            case OK: {
                color = Color.GREEN;
                str = "OK";
            } break;

            case HandheldNotConnected: {
                color = Color.RED;
                str = "Handheld not connected";
                retryConnectionButton.setVisibility(View.VISIBLE);
            } break;

            default: {
                color = Color.RED;
                str = "Error";
            } break;
        }
        if (text == null) {
            finalStr = str;
        } else {
            finalStr = str + " " + text;
        }
        Handler mainHandler = new Handler(Looper.getMainLooper());
        mainHandler.post(() -> {
            mStatus.setTextColor(color);
            mStatus.setText(finalStr);
        });
    }

    private String accStr(int accuracy) {
        if (accuracy == 3) { return "High"; }
        if (accuracy == 2) { return "Medium"; }
        if (accuracy == 1) { return "Low"; }
        return "Unknown (" + accuracy + ")";
    }

    public void heartSampleTaken(float heartRate, int accuracy, int steps) {
        Handler mainHandler = new Handler(Looper.getMainLooper());
        mainHandler.post(() -> {
            mHeartRate.setText("Heart rate: " + heartRate + "  Accuracy: " + accStr(accuracy) + "  Steps: " + steps);
            notification(heartRate, accuracy, steps);
        });
    }

    private void notification(float heartRate, int accuracy, int steps) {
        int notificationId = 001;
        // Build intent for notification content
        Intent viewIntent = new Intent(this, MainActivity.class);
        //viewIntent.putExtra(EXTRA_EVENT_ID, eventId);
        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, 0, viewIntent, 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_full_sad)
                        .setContentTitle("Sensor stats:")
                        .setContentText("Heart rate: " + heartRate + ", Steps: " + steps)
                        .setContentIntent(viewPendingIntent);

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        // Build the notification and issues it with notification manager.
        Notification n = notificationBuilder.build();
        //n.flags |= Notification.
        notificationManager.notify(notificationId, n);
        System.out.println("Notified");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
       // savedInstanceState.putString(KEY, KEY);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }
}
