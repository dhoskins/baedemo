package com.futureplatforms.baedemo;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import com.futureplatforms.baedemo.broker.BAEView;
import com.futureplatforms.baedemo.broker.SensorNetworkBroker;

/**
 * Created by douglashoskins on 15/07/2016.
 */
public class BAEService extends Service implements BAEView {
    private SensorNetworkBroker snb;
    private WearableMessageSender wms;
    private BAESensorObtainer sensors;

    protected static boolean IS_RUNNING = false;

    @Override
    public void onDestroy() {
        super.onDestroy();
        IS_RUNNING = false;
        System.out.println("BAEService.onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("BAEService.onStartCommand");
        String id = intent.getStringExtra("id");
        snb = new SensorNetworkBroker(this, id);
        wms = new WearableMessageSender(this);
        tryRemoteConnect();
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IS_RUNNING = true;
        System.out.println("BAEService.onCreate");
    }

    private void startSensors() {
        if (sensors == null) {
            sensors = new BAESensorObtainer(this);
        }
        sensors.startListening(snb);
    }


    private void tryRemoteConnect() {
        AsyncTask.execute(() -> {
            wms.setup(() -> {
                snb.setMessageSender(wms);
                startSensors();
            }, (String s) -> {
                // Connection failed
                setStatus(Status.HandheldNotConnected, s);
                stopSelf();
            });
        });
    }

    @Override
    public void setStatus(Status s) {
        setStatus(s, null);
    }

    @Override
    public void setStatus(Status s, String text) {
        Intent intent = new Intent("BAEStatus");
        intent.putExtra("Status", s.ordinal());
        intent.putExtra("Text", text == null ? "" : text);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void heartSampleTaken(float heartRate, int accuracy, int steps) {
        Intent intent = new Intent("BAESensorData");
        intent.putExtra("HeartRate", heartRate);
        intent.putExtra("Accuracy", accuracy);
        intent.putExtra("Steps", steps);
        System.out.println("Sending broadcast");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
