package com.futureplatforms.baedemo.broker;

/**
 * Created by douglashoskins on 12/07/2016.
 */
public interface ReceiveHeartRate {
    void acceptHeartRate(float heartRate, int accuracy);
    void acceptSteps(float numSteps);
}
