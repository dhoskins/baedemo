package com.futureplatforms.baedemo;

/**
 * Created by douglashoskins on 12/07/2016.
 */
public interface RunnableParam<T> {
    void apply(T t);
}
