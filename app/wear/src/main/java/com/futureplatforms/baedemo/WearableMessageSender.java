package com.futureplatforms.baedemo;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.futureplatforms.baedemo.broker.MessageSender;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.Set;

/**
 * Created by douglashoskins on 11/07/2016.
 */
public class WearableMessageSender implements MessageSender {
    public static final String BAE_HEARTRATE_MESSAGE_PATH = "/bae_heartrate";
    private static final String BAE_HEARTRATE_CAPABILITY_NAME = "bae_heartrate";

    private String transcriptionNodeId;
    private GoogleApiClient googleApiClient;

    public WearableMessageSender(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context).addApi(Wearable.API).build();
    }

    public void setup(Runnable onSuccess, RunnableParam<String> onError) {
        ConnectionResult connRes = googleApiClient.blockingConnect();
        Handler mainHandler = new Handler(Looper.getMainLooper());

        if (connRes.isSuccess()) {
            System.out.println("Connected to google API Client : SUCCESS");
            CapabilityApi.GetCapabilityResult res = Wearable.CapabilityApi.getCapability(
                    googleApiClient,
                    BAE_HEARTRATE_CAPABILITY_NAME,
                    CapabilityApi.FILTER_REACHABLE).await();
            Set<Node> connectedNodes = res.getCapability().getNodes();
            transcriptionNodeId = pickBestNodeId(connectedNodes);
            if (transcriptionNodeId != null && !"".equals(transcriptionNodeId)) {
                System.out.println("Connected to node : SUCCESS " + transcriptionNodeId);
                mainHandler.post(onSuccess);
            } else {
                System.out.println("Connected to node : FAIL");
                mainHandler.post(() -> onError.apply("Failed to connect to node"));
            }
        } else {
            System.out.println("Connected to google API Client : FAIL " + connRes.toString());
            mainHandler.post(() -> onError.apply("Failed to connect Google API Client: " + connRes.toString()));
        }
    }

    private String pickBestNodeId(Set<Node> nodes) {
        String bestNodeId = null;
        // Find a nearby node or pick one arbitrarily
        for (Node node : nodes) {
            if (node.isNearby()) {
                System.out.println("Found nearby node");
                return node.getId();
            }
            bestNodeId = node.getId();
        }
        System.out.println("Did not find nearby node");
        return bestNodeId;
    }

    @Override
    public void sendMessage(byte[] data, final Runnable onSuccess, RunnableParam<String> onError) {
        if (transcriptionNodeId != null) {
            Wearable.MessageApi.sendMessage(googleApiClient, transcriptionNodeId,
                    BAE_HEARTRATE_MESSAGE_PATH, data).setResultCallback(
                    new ResultCallback() {
                        @Override
                        public void onResult(@NonNull Result result) {
                            System.out.println("Message Result: " + result.getStatus());
                            if (!result.getStatus().isSuccess()) {
                                // Failed to send message
                                onError.apply("Failed to send message: " + result.getStatus().toString());
                            } else {
                                onSuccess.run();
                            }
                        }
                    }
            );
        } else {
            // Unable to retrieve node with transcription capability
            onError.apply("No node available to send message!");
        }
    }
}
