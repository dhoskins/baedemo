package com.futureplatforms.baedemo.broker;

/**
 * Created by douglashoskins on 12/07/2016.
 */
public interface BAEView {
    enum Status { OK, HandheldNotConnected, Error }
    void setStatus(Status s);
    void setStatus(Status s, String text);
    void heartSampleTaken(float heartRate, int accuracy, int steps);
}
