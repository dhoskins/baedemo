package com.futureplatforms.baedemo.broker;

import com.futureplatforms.baedemo.broker.BAEView.Status;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by douglashoskins on 12/07/2016.
 */
public class SensorNetworkBroker implements ReceiveHeartRate {
    private MessageSender ms;
    private BAEView v;
    private int offsetSteps = 0;
    private boolean hasSetOffset = false;
    private int steps = 0;
    private Date lastDate;
    private String id;

    public SensorNetworkBroker(BAEView v, String id) {
        this.v = v;
        this.id = id;
    }

    @Override
    public void acceptHeartRate(float heartRate, int accuracy) {
        v.heartSampleTaken(heartRate, accuracy, steps);
        if (ms == null) {
            v.setStatus(Status.HandheldNotConnected);
            return;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        try {
            dos.writeUTF(id);
            dos.writeFloat(heartRate);
            dos.writeInt(accuracy);
            dos.writeInt(steps);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("SNB. sending heart message");
        ms.sendMessage(
                baos.toByteArray(),
                () -> v.setStatus(Status.OK),
                (String s) -> v.setStatus(Status.Error, s));
    }

    @Override
    public void acceptSteps(float numSteps) {
        System.out.println("AcceptSteps: " + numSteps);
        System.out.println("OffsetSteps: " + offsetSteps);
        System.out.println("this.steps: " + steps);
        // 4533
        if (!hasSetOffset) {
            hasSetOffset = true;
            this.offsetSteps = (int) numSteps;
            this.lastDate = new Date();
        }
        if (new Date().getDay() != lastDate.getDay()) {
            // we are in a new day, reset the step count;
            this.offsetSteps = (int) numSteps;
        }
        this.steps = (int) numSteps - this.offsetSteps;
    }

    public void setMessageSender(MessageSender ms) {
        this.ms = ms;
    }
}
