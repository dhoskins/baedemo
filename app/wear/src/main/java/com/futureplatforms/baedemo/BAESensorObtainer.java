package com.futureplatforms.baedemo;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.futureplatforms.baedemo.broker.ReceiveHeartRate;

/**
 * Created by douglashoskins on 11/07/2016.
 */
public class BAESensorObtainer implements SensorEventListener {
    private static final int TYPE_PPG = 65545;
    private Sensor heartRateSensor, stepCounterSensor;
    private SensorManager sensorManager;
    private ReceiveHeartRate receiveHeartRate;
    private static final long TIME_BETWEEN_UPDATES = 3000;
    private long lastUpdate = 0;

    public BAESensorObtainer(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        heartRateSensor = sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        stepCounterSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
    }

    public void startListening(ReceiveHeartRate receiveHeartRate) {
        this.receiveHeartRate = receiveHeartRate;
        sensorManager.registerListener(this, heartRateSensor, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, stepCounterSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stopListening() {
        this.receiveHeartRate = null;
        if (heartRateSensor != null) {
            sensorManager.unregisterListener(this, heartRateSensor);
        }
        if (stepCounterSensor != null) {
            sensorManager.unregisterListener(this, stepCounterSensor);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (receiveHeartRate != null && sensorEvent.sensor == heartRateSensor) {
            long now = System.currentTimeMillis();
            if ((now - lastUpdate) > TIME_BETWEEN_UPDATES) {
                receiveHeartRate.acceptHeartRate(sensorEvent.values[0], sensorEvent.accuracy);
                lastUpdate = now;
            }
        } else if (receiveHeartRate != null && sensorEvent.sensor == stepCounterSensor) {
            // must be the step counter sensor
            receiveHeartRate.acceptSteps(sensorEvent.values[0]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) { }
}
