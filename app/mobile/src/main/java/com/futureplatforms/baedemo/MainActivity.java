package com.futureplatforms.baedemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    TextView statusConn, statusServer;
    EditText edittext_server;
    Button button_change_server;

    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.setDebug(true);

        setContentView(R.layout.activity_main);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        statusConn = (TextView) findViewById(R.id.status_connection);
        statusServer = (TextView) findViewById(R.id.status_server);
        edittext_server = (EditText) findViewById(R.id.edittext_server);
        button_change_server = (Button) findViewById(R.id.button_change_server);
        button_change_server.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setServer(edittext_server.getText().toString());
            }
        });

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        if (!sharedPref.contains("server")) {
            setServer("http://10.10.1.109:5432/heart");
        }
        statusServer.setText("Current server URL: " + sharedPref.getString("server", null));
        if (!MessageReceiveService.IS_RUNNING) {
            startService(new Intent(this, MessageReceiveService.class));
        }
    }

    private void setServer(String str) {
        Editor editor = sharedPref.edit();
        editor.putString("server", str);
        editor.commit();
        statusServer.setText("Current server URL: " + sharedPref.getString("server", null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private BroadcastReceiver statusRec = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            Handler mainHandler = new Handler(Looper.getMainLooper());
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    statusConn.setText(intent.getStringExtra("status"));
                }
            });
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(statusRec, new IntentFilter("BAEStatus"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(statusRec);
    }
}
