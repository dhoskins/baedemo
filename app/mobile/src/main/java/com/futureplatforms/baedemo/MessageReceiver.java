package com.futureplatforms.baedemo;

/**
 * Created by douglashoskins on 17/07/2016.
 */
public class MessageReceiver {
    private MessageReceiver() {

    }

    private static MessageReceiver instance;
    public static MessageReceiver getInstance() {
        if ( instance == null ) {
            instance = new MessageReceiver();
        }
        return instance;
    }
}
