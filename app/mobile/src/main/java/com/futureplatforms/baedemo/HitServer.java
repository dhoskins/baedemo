package com.futureplatforms.baedemo;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by douglashoskins on 13/07/2016.
 */
public class HitServer {

    public void send(String url, String id, float heartRate, int accuracy, int steps) throws IOException {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("userId", id)
                .add("heartRate", "" + heartRate)
                .add("accuracy", "" + accuracy)
                .add("steps", "" + steps)
                .build();
        Request request = new Request.Builder()
                //.url("http://10.10.10.6:5432/heart")
                .url(url)
                .post(formBody)
                .build();

        try {
            Response response = client.newCall(request).execute();

            System.out.println("Response: " + response.message());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
