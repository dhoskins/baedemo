package com.futureplatforms.baedemo;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.wearable.MessageApi.MessageListener;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Created by douglashoskins on 17/07/2016.
 */
public class MessageReceiveService extends Service implements MessageListener {
    private static final String BAE_HEARTRATE_MESSAGE_PATH = "/bae_heartrate";

    protected static boolean IS_RUNNING = false;
    private GoogleApiClient googleApiClient;
    private Map<String, Integer> map = new HashMap<>();

    private SharedPreferences sharedPref;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) { return null; }

    @Override
    public void onCreate() {
        super.onCreate();
        IS_RUNNING = true;
        System.out.println("MessageReceiveService.onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IS_RUNNING = true;
        System.out.println("MessageReceiveService.onDestroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("MessageReceiveService.onStartCommand");
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        googleApiClient = new Builder(this).addApi(Wearable.API).build();
        googleApiClient.registerConnectionCallbacks(new ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                System.out.println("Google play services connected");
                Wearable.MessageApi.addListener(googleApiClient, MessageReceiveService.this);
            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        });
        googleApiClient.connect();
        return START_STICKY;
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        System.out.println("Message received");
        if (messageEvent.getPath().equals(BAE_HEARTRATE_MESSAGE_PATH)) {
            byte[] bytes = messageEvent.getData();
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            DataInputStream dis = new DataInputStream(bais);
            try {
                final String id = dis.readUTF();
                final float heartRate = dis.readFloat();
                final int accuracy = dis.readInt();
                final int steps = dis.readInt();

                if (!map.containsKey(id)) {
                    map.put(id, 0);
                } else {
                    map.put(id, map.get(id) + 1);
                }

                Intent intent = new Intent("BAEStatus");
                String str = "";
                for (Entry<String, Integer> e : map.entrySet()) {
                    str += "Watch " + e.getKey() + ": " + e.getValue() + " messages received\n\n";
                }
                intent.putExtra("status", str);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String url = sharedPref.getString("server", null);
                            new HitServer().send(url, id, heartRate, accuracy, steps);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
